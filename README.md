# TimeFlo
Copyright &copy; 2021 *Daniel Gerendasy*

TimeFlo is an implementation of a
[Pomodoro&reg;](https://en.wikipedia.org/wiki/Pomodoro_Technique)-like
timer for breaking out of flow state.

TimeFlo is a Windows GUI application that manages productivity (work) and break sessions by alternating between fixed-duration timers. When a timer finishes, an [alert sound](#alert-sound) will play. Starting and stopping TimeFlo is as easy as clicking a button.

Productivity and break session durations are configurable in the GUI.

## Status and Roadmap

TimeFlo MVP is functional but has yet to complete V&V.

* [x] Requirements complete.
* [x] Project plan complete.
* [x] Design complete.
* [x] Implementation complete.
* [x] Validation complete.

## Build and Run

1. Install Visual Studio 2019
    * Install Windows .NET Desktop modules
2. Open the TimeFlo.sln solution file in Visual Studio
3. Build and run the project from Visual Studio

## Development Docs

Development documentation is available for TimeFlo, including:

* [Requirements Specification](docs/reqs.md)
* [Project Plan](docs/plan.md)
* [Design Doc](docs/design.md)
* [V&amp;V Report](docs/vnv.md)

## Alert Sound

Credits to "UI Confirmation Alert, D1.wav" by InspectorJ (www.jshaw.co.uk) of Freesound.org for the [alert audio file](./src/Properties/alert.wav) used in this project.
