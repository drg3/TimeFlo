﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Threading;

namespace TimeFlo.Tests
{
    [TestClass]
    public class TimeFloUnitTest
    {
        [TestMethod("Zero session durations")]
        public void TestTimer1()
        {
            TestTimeFlo(2, 0, 0);
        }

        [TestMethod("Small session durations")]
        public void TestTimer2()
        {
            TestTimeFlo(30, 15, 5);
        }

        [TestMethod("Long session durations")]
        public void TestTimer3()
        {
            TestTimeFlo(3, 60, 120);
        }

        [TestMethod("Abrupt stop during break")]
        public void TestTimer4()
        {
            // productivity will finish and then break will start and never finish
            // so expected number of finished timers is 1
            Assert.AreEqual(TestTimeFloStop(5000, 10, 0), 1);
        }

        [TestMethod("Abrupt stop during productivity")]
        public void TestTimer5()
        {
            // productivity will start and never finish
            // so expected number of finished timers is 0
            Assert.AreEqual(TestTimeFloStop(5000, 0, 10), 0);
        }

        private void TestTimeFlo(int stopAfter, int breakDurationSeconds, int prodDurationSeconds)
        {
            TimeFlo timeflo = new TimeFlo();
            int timerFinishedCount = 0;

            // construct expected timer duration
            // for every multiple of 2, add one break and productivity timer duration
            // if stopAfter is odd, then add one final productivity timer duration to the total
            int totalDurationSeconds = ((stopAfter / 2) * (breakDurationSeconds + prodDurationSeconds)) + (prodDurationSeconds * (stopAfter % 2));

            // set durations
            timeflo.SetBreakTimerInterval(breakDurationSeconds);
            timeflo.SetProductivityTimerInterval(prodDurationSeconds);

            // hook callbacks
            // if productivity is called first and the break is called next, then timeflo will stop after the expected duration
            timeflo.OnBreakTimerFinished += (tf) =>
            {
                // break should happen every odd count
                Assert.IsTrue(timerFinishedCount % 2 == 1);
                ++timerFinishedCount;
            };
            timeflo.OnProductivityTimerFinished += (tf) =>
            {
                // productivity should happen every even count
                Assert.IsTrue(timerFinishedCount % 2 == 0);
                ++timerFinishedCount;
            };

            // start
            Stopwatch sw = Stopwatch.StartNew();
            timeflo.Start();

            // wait for timer to run timers enough times
            while (timerFinishedCount < stopAfter)
                ;

            timeflo.Stop();
            sw.Stop();

            // assert timeflo stopped
            Assert.IsNull(timeflo.GetTimerState());

            // assert timeflo lasted for one break and one productivity session to within 500 ms
            Assert.IsTrue(Math.Abs(sw.Elapsed.TotalSeconds - totalDurationSeconds) < 0.5);
        }

        private int TestTimeFloStop(int breakAfterMs, int breakDurationSeconds, int prodDurationSeconds)
        {
            TimeFlo timeflo = new TimeFlo();
            Stopwatch sw = new Stopwatch();
            int timerFinishedCount = 0;

            // set durations
            timeflo.SetBreakTimerInterval(breakDurationSeconds);
            timeflo.SetProductivityTimerInterval(prodDurationSeconds);

            // hook callbacks
            // if productivity is called first and the break is called next, then timeflo will stop after the expected duration
            timeflo.OnBreakTimerFinished += (tf) =>
            {
                // break should happen every odd count
                Assert.IsTrue(timerFinishedCount % 2 == 1);
                ++timerFinishedCount;
            };
            timeflo.OnProductivityTimerFinished += (tf) =>
            {
                // productivity should happen every even count
                Assert.IsTrue(timerFinishedCount % 2 == 0);
                ++timerFinishedCount;
            };

            // start
            sw.Start();
            timeflo.Start();

            // wait for timer to reach break time
            while (sw.ElapsedMilliseconds < breakAfterMs)
                ;

            timeflo.Stop();
            sw.Stop();

            // assert timeflo stopped
            Assert.IsNull(timeflo.GetTimerState());

            // assert timeflo lasted for one break and one productivity session to within 500 ms
            Assert.IsTrue(Math.Abs(sw.Elapsed.TotalMilliseconds - breakAfterMs) < 500);

            return timerFinishedCount;
        }
    }
}
