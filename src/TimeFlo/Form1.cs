﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeFlo
{
    public partial class Form1 : Form
    {
        private TimeFlo timeFlo;
        private IWavePlayer alertWavePlayer;
        private AudioFileReader alertWaveFileReader;

        public Form1()
        {
            InitializeComponent();

            // initialize local TimeFlo object
            timeFlo = new TimeFlo();
            timeFlo.OnBreakTimerFinished += TimeFlo_OnBreakTimerFinished;
            timeFlo.OnProductivityTimerFinished += TimeFlo_OnProductivityTimerFinished;
            SetTimeDurations();

            // initialize our alert wave player
            alertWavePlayer = new WaveOut();
            alertWaveFileReader = new AudioFileReader("Properties/alert.wav");
            alertWavePlayer.Init(alertWaveFileReader);
            SetAlertVolume();
        }

        #region TimeFlo Handlers

        // Invoked when the productivity timer finishes
        private void TimeFlo_OnProductivityTimerFinished(TimeFlo obj)
        {
            // play the alert sound to indicate to user the transition to taking a break
            PlayAlert();
        }

        // Invoked when the break timer finishes
        private void TimeFlo_OnBreakTimerFinished(TimeFlo obj)
        {
            // play the alert sound to indicate to user the transition to working
            PlayAlert();
        }

        #endregion

        #region TimeFlo State

        // Sets the local TimeFlo instance's productivity and break timer durations to their respective input component's value
        private void SetTimeDurations()
        {
            // we multiply the value by 60 because the SetTimerInterval functions want the value in seconds
            // but we ask the user to enter a value in minutes
            this.timeFlo.SetProductivityTimerInterval((int)inputProductivityTimerDuration.Value * 60);
            this.timeFlo.SetBreakTimerInterval((int)inputProductivityTimerDuration.Value * 60);
        }

        #endregion

        #region Alert

        // Plays the alert once
        private void PlayAlert()
        {
            alertWaveFileReader.Seek(0, System.IO.SeekOrigin.Begin);
            alertWavePlayer.Play();
        }

        // Updates the alert player's volume to the volume set in the timer alert volume input component
        private void SetAlertVolume()
        {
            this.alertWavePlayer.Volume = inputTimerAlertVolume.Value / 10f;
        }

        #endregion

        #region UI State

        // Changes the GUI state to reflect that of when TimeFlo is not running
        // This effectively hides/resets the status, re-enables the inputs, and disables the stop button
        private void SetStateTimeFloOff()
        {
            buttonStop.Enabled = false;
            buttonStart.Enabled = true;
            inputBreakTimerDuration.Enabled = true;
            inputProductivityTimerDuration.Enabled = true;
            labelStatusTime.Visible = false;
            labelStatus.Text = "TimeFlo not running";
        }

        // Changes the GUI state to reflect that of when TimeFlo is running
        // This effectively disables the inputs and start button, enables the stop button
        // and updates the status components to the respective input string
        private void SetStateTimeFloOn(string status, string statusTime)
        {
            buttonStop.Enabled = true;
            buttonStart.Enabled = false;
            inputBreakTimerDuration.Enabled = false;
            inputProductivityTimerDuration.Enabled = false;
            labelStatus.Text = status;
            labelStatusTime.Visible = true;
            labelStatusTime.Text = statusTime;
        }

        #endregion

        #region UI Control Handlers

        // Invoked every timerStatus tick
        // Updates the current UI's status components to reflect current TimeFlo state
        private void timerStatus_Tick(object sender, EventArgs e)
        {
            var state = this.timeFlo.GetTimerState();
            if (state == null)
            {
                SetStateTimeFloOff();
            }
            else
            {
                var status = state.Timer == TimeFloTimer.Productivity ? "It's work time!" : "It's break time!";
                var secondsLeft = TimeSpan.FromSeconds(state.SecondsLeft);
                SetStateTimeFloOn(status, secondsLeft.ToString());
            }
        }

        // Invoked when the productivity timer duration input's value changes
        private void inputProductivityTimerDuration_ValueChanged(object sender, EventArgs e)
        {
            SetTimeDurations();
        }

        // Invoked when the break timer duration input's value changes
        private void inputBreakTimerDuration_ValueChanged(object sender, EventArgs e)
        {
            SetTimeDurations();
        }

        // Invoked when the timer alert volume input's value changes
        private void inputTimerAlertVolume_Scroll(object sender, EventArgs e)
        {
            SetAlertVolume();
        }

        // Invoked when the user presses the start button
        private void buttonStart_Click(object sender, EventArgs e)
        {
            this.timeFlo.Start();
        }

        // Invoked when the user presses the stop button
        private void buttonStop_Click(object sender, EventArgs e)
        {
            this.timeFlo.Stop();
        }

        #endregion

    }
}
