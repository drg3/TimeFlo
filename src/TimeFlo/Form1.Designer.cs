﻿
namespace TimeFlo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.inputTimerAlertVolume = new System.Windows.Forms.TrackBar();
            this.labelTimerAlertVolume = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.labelBreakTimerDuration = new System.Windows.Forms.Label();
            this.inputBreakTimerDuration = new System.Windows.Forms.NumericUpDown();
            this.labelProductivityTimerDuration = new System.Windows.Forms.Label();
            this.inputProductivityTimerDuration = new System.Windows.Forms.NumericUpDown();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelStatusTime = new System.Windows.Forms.Label();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputTimerAlertVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputBreakTimerDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputProductivityTimerDuration)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.inputTimerAlertVolume);
            this.groupBox1.Controls.Add(this.labelTimerAlertVolume);
            this.groupBox1.Controls.Add(this.buttonStart);
            this.groupBox1.Controls.Add(this.buttonStop);
            this.groupBox1.Controls.Add(this.labelBreakTimerDuration);
            this.groupBox1.Controls.Add(this.inputBreakTimerDuration);
            this.groupBox1.Controls.Add(this.labelProductivityTimerDuration);
            this.groupBox1.Controls.Add(this.inputProductivityTimerDuration);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(800, 112);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // inputTimerAlertVolume
            // 
            this.inputTimerAlertVolume.Location = new System.Drawing.Point(198, 72);
            this.inputTimerAlertVolume.Name = "inputTimerAlertVolume";
            this.inputTimerAlertVolume.Size = new System.Drawing.Size(120, 45);
            this.inputTimerAlertVolume.TabIndex = 7;
            this.inputTimerAlertVolume.Value = 5;
            this.inputTimerAlertVolume.Scroll += new System.EventHandler(this.inputTimerAlertVolume_Scroll);
            // 
            // labelTimerAlertVolume
            // 
            this.labelTimerAlertVolume.AutoSize = true;
            this.labelTimerAlertVolume.Location = new System.Drawing.Point(13, 76);
            this.labelTimerAlertVolume.Name = "labelTimerAlertVolume";
            this.labelTimerAlertVolume.Size = new System.Drawing.Size(95, 13);
            this.labelTimerAlertVolume.TabIndex = 6;
            this.labelTimerAlertVolume.Text = "Timer Alert Volume";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(570, 20);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(106, 80);
            this.buttonStart.TabIndex = 5;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Location = new System.Drawing.Point(682, 20);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(106, 80);
            this.buttonStop.TabIndex = 4;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // labelBreakTimerDuration
            // 
            this.labelBreakTimerDuration.AutoSize = true;
            this.labelBreakTimerDuration.Location = new System.Drawing.Point(13, 48);
            this.labelBreakTimerDuration.Name = "labelBreakTimerDuration";
            this.labelBreakTimerDuration.Size = new System.Drawing.Size(152, 13);
            this.labelBreakTimerDuration.TabIndex = 3;
            this.labelBreakTimerDuration.Text = "Break Timer Duration (minutes)";
            // 
            // inputBreakTimerDuration
            // 
            this.inputBreakTimerDuration.Location = new System.Drawing.Point(198, 46);
            this.inputBreakTimerDuration.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.inputBreakTimerDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputBreakTimerDuration.Name = "inputBreakTimerDuration";
            this.inputBreakTimerDuration.Size = new System.Drawing.Size(120, 20);
            this.inputBreakTimerDuration.TabIndex = 2;
            this.inputBreakTimerDuration.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.inputBreakTimerDuration.ValueChanged += new System.EventHandler(this.inputBreakTimerDuration_ValueChanged);
            // 
            // labelProductivityTimerDuration
            // 
            this.labelProductivityTimerDuration.AutoSize = true;
            this.labelProductivityTimerDuration.Location = new System.Drawing.Point(13, 20);
            this.labelProductivityTimerDuration.Name = "labelProductivityTimerDuration";
            this.labelProductivityTimerDuration.Size = new System.Drawing.Size(179, 13);
            this.labelProductivityTimerDuration.TabIndex = 1;
            this.labelProductivityTimerDuration.Text = "Productivity Timer Duration (minutes)";
            // 
            // inputProductivityTimerDuration
            // 
            this.inputProductivityTimerDuration.Location = new System.Drawing.Point(198, 18);
            this.inputProductivityTimerDuration.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.inputProductivityTimerDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputProductivityTimerDuration.Name = "inputProductivityTimerDuration";
            this.inputProductivityTimerDuration.Size = new System.Drawing.Size(120, 20);
            this.inputProductivityTimerDuration.TabIndex = 0;
            this.inputProductivityTimerDuration.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.inputProductivityTimerDuration.ValueChanged += new System.EventHandler(this.inputProductivityTimerDuration_ValueChanged);
            // 
            // labelStatus
            // 
            this.labelStatus.Location = new System.Drawing.Point(12, 9);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(772, 23);
            this.labelStatus.TabIndex = 1;
            this.labelStatus.Text = "TimeFlo not running";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStatusTime
            // 
            this.labelStatusTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatusTime.Location = new System.Drawing.Point(12, 32);
            this.labelStatusTime.Name = "labelStatusTime";
            this.labelStatusTime.Size = new System.Drawing.Size(772, 54);
            this.labelStatusTime.TabIndex = 2;
            this.labelStatusTime.Text = "10:00";
            this.labelStatusTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelStatusTime.Visible = false;
            // 
            // timerStatus
            // 
            this.timerStatus.Enabled = true;
            this.timerStatus.Interval = 500;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 267);
            this.Controls.Add(this.labelStatusTime);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "TimeFlo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputTimerAlertVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputBreakTimerDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputProductivityTimerDuration)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelBreakTimerDuration;
        private System.Windows.Forms.NumericUpDown inputBreakTimerDuration;
        private System.Windows.Forms.Label labelProductivityTimerDuration;
        private System.Windows.Forms.NumericUpDown inputProductivityTimerDuration;
        private System.Windows.Forms.TrackBar inputTimerAlertVolume;
        private System.Windows.Forms.Label labelTimerAlertVolume;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelStatusTime;
        private System.Windows.Forms.Timer timerStatus;
    }
}

