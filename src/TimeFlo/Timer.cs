﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeFlo
{
    public class Timer
    {
        // Event triggered when the timer has elapsed the given Interval
        public event EventHandler<System.Timers.ElapsedEventArgs> Elapsed;

        // The number of seconds before the timer triggers the Elapsed event
        public int IntervalSeconds { get; set; }

        // The number of seconds since the stopwatch timer has started.
        public int ElapsedSeconds => (int)stopwatch.Elapsed.TotalSeconds;
        
        // Whether or not the timer is currently running
        public bool Enabled => tickTimer.Enabled;

        
        private System.Timers.Timer tickTimer;
        private Stopwatch stopwatch;

        public Timer()
        {
            // initialize local timer to tick every 5 ms
            // this helps us raise an event when the stopwatch has surpassed the
            // target interval
            tickTimer = new System.Timers.Timer(5);
            tickTimer.Elapsed += Timer_Elapsed;
            tickTimer.AutoReset = true;

            // begin stopwatch to keep track of time
            stopwatch = new Stopwatch();
        }

        // Triggered every tick
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // if enough time has passed, trigger Elapsed event and reset
            if (stopwatch.Elapsed.TotalSeconds >= IntervalSeconds)
            {
                Elapsed?.Invoke(this, e);
                stopwatch.Restart();
            }
        }

        // Starts the timer
        public void Start()
        {
            tickTimer.Start();
            stopwatch.Restart();
        }

        // Stops the timer
        public void Stop()
        {
            tickTimer.Stop();
            stopwatch.Stop();
        }
    }
}
