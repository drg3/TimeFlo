# TimeFlo
Copyright &copy; 2021 *Daniel Gerendasy*

Here lies the WinForms GUI source code for TimeFlo.

## Alert Sound

Credits to "UI Confirmation Alert, D1.wav" by InspectorJ (www.jshaw.co.uk) of Freesound.org for the [alert audio file](./Properties/alert.wav) used in this project.
