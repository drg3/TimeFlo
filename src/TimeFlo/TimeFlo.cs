﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeFlo
{
    public enum TimeFloTimer
    {
        Productivity,
        Break
    }

    public class TimeFloTimerState
    {
        // Which timer this state refers to
        public TimeFloTimer Timer { get; }

        // How many seconds are left before the timer triggers its Elapsed event
        public int SecondsLeft { get; }

        public TimeFloTimerState(TimeFloTimer timer, int secondsLeft)
        {
            this.Timer = timer;
            this.SecondsLeft = secondsLeft;
        }
    }

    public class TimeFlo
    {
        // Event raised when the productivity timer finishes.
        public event Action<TimeFlo> OnProductivityTimerFinished;

        // Event raised when the break timer finishes.
        public event Action<TimeFlo> OnBreakTimerFinished;

        // Timer object representing productivity timer
        private Timer productivityTimer;

        // Timer object representing break timer
        private Timer breakTimer;

        // Constructor
        public TimeFlo()
        {
            // initialize timers with default intervals and elapsed handlers
            productivityTimer = new Timer();
            productivityTimer.Elapsed += ProductivityTimer_Elapsed;

            breakTimer = new Timer();
            breakTimer.Elapsed += BreakTimer_Elapsed;
        }

        #region User Controls

        // Sets the number of seconds for the productivity timer to run for
        public void SetProductivityTimerInterval(int seconds)
        {
            productivityTimer.IntervalSeconds = seconds;
        }

        // Sets the number of seconds for the break timer to run for
        public void SetBreakTimerInterval(int seconds)
        {
            breakTimer.IntervalSeconds = seconds;
        }

        // Returns the current state of the currently running timer
        // If no timer is running, returns null
        public TimeFloTimerState GetTimerState()
        {
            if (productivityTimer.Enabled)
                return new TimeFloTimerState(TimeFloTimer.Productivity, productivityTimer.IntervalSeconds - productivityTimer.ElapsedSeconds);
            else if (breakTimer.Enabled)
                return new TimeFloTimerState(TimeFloTimer.Break, breakTimer.IntervalSeconds - breakTimer.ElapsedSeconds);

            return null;
        }

        // Begins the TimeFlo process by starting the productivity timer
        public void Start()
        {
            // first, ensure that both timers have stopped
            Stop();

            // then, start the productivity timer
            StartProductivityTimer();
        }

        // Stops the TimeFlo process by stopping any running timer
        public void Stop()
        {
            // check if timers are active and stop if so
            if (productivityTimer.Enabled)
                productivityTimer.Stop();
            if (breakTimer.Enabled)
                breakTimer.Stop();
        }

        #endregion

        #region Timer Handlers

        // Triggered when the productivity timer hits 0
        private void ProductivityTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // start the break timer
            StartBreakTimer();

            // stop this timer
            productivityTimer.Stop();

            // raise productivity timer finished event
            OnProductivityTimerFinished?.Invoke(this);
        }

        // Triggered when the break timer hits 0
        private void BreakTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // start the break timer
            StartProductivityTimer();

            // stop this timer
            breakTimer.Stop();

            // raise productivity timer finished event
            OnBreakTimerFinished?.Invoke(this);
        }

        // Starts the productivity timer
        private void StartProductivityTimer()
        {
            // start timer
            productivityTimer.Start();
        }

        // Starts the break timer
        private void StartBreakTimer()
        {
            // start timer
            breakTimer.Start();
        }

        #endregion
    }
}
