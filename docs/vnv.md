# The TimeFlo Project: Validation and Verification
*Daniel Gerendasy 2021*

## Introduction

This document describes how the TimeFlo project will be verified and validated to ensure that it meets the requirements specified in the docs and meets the customers needs.

### Purpose and Scope

To outline the process for V&V.

### Target Audience

This document is relevant to anyone working on the TimeFlo project.

### Terms and Definitions

 * Cycle: One successfully completed productivity and break session
 * TimeFlo process: Refers to the running of the two alternating timers

## Test Plan Description

The following tests will help ensure that the TimeFlo application will function according to the project's requirements.

### Scope Of Testing

This plan will test the TimeFlo user interface and underlying TimeFlo/timer logic.

### Testing Schedule

Testing will occur after each code change and before the project deadline of November 9th, 2021.

### Release Criteria

Within the scope of our tests, the system must never fault.

## Unit Testing

Unit tests will be performed on the TimeFlo and Timer components.

### Strategy

The strategy for unit testing the two components is to cover all methods in the TimeFlo and Timer classes. Since the Timer class is utilized exclusively and extensively by the TimeFlo class, we assume by testing TimeFlo we will also be testing Timer. The user interface will be tested in conjunction with the TimeFlo and Timer components in the [System Testing](#system-testing) section.

The unit tests will test the accuracy of TimeFlo after short and long durations. It will also test to ensure the alternating of the productivity and break timers, with the productivity timer always starting first. The unit tests will also test that the stopping of the timer during either session will function as expected.

I will not be testing negative session durations because the user interface is required to not allow the user to enter a negative number for the session durations.

### Zero Session Durations Test

This test will run the TimeFlo timer for one cycle with the break and productivity session durations set to zero seconds.

The expected result is that this timer will exit almost immediately as both timers will fire one after the other before exiting.

### Small Session Durations Test

This test will run the TimeFlo timer for 30 cycles with the break and productivity session durations set to 15 and 5 seconds, respectively.

The expected result is that the timer will successfully alternative between the two timers and exit after the final productivity timer finishes. The expected total duration of this test is 5 minutes +- 500 ms.

### Long Session Durations Test

This test will run the TimeFlo timer for one cycle and one final productivity session with the break and productivity session durations set to 60 and 120 seconds, respectively.

The expected result is that the timer will successfully alternative between the two timers and exit after the first and final break timer finishes. The expected total duration of this test is 5 minutes +- 500 ms.

### Abrupt Stop During Break Test

This test will run the TimeFlo timer for 5 seconds with the productivity timer set to 0 seconds and the break timer set to 10 seconds. After 5 seconds, the timer will be counting down the break timer and the test will abruptly stop the TimeFlo timer.

The expected result is for the timer to simply stop. It will not fire the break timer finished callback.

### Abrupt Stop During Productivity Test

This test will run the TimeFlo timer for 5 seconds with the productivity timer set to 10 seconds and the break timer set to 0 seconds. After 5 seconds, the timer will be counting down the productivity timer and the test will abruptly stop the TimeFlo timer.

The expected result is for the timer to simply stop. It will not fire the productivity timer finished callback.

## System Testing

This section will outline the various system tests to be performed.

### TimeFlo Launches

Launch the application.

Expected result is that it launches succesfully and a GUI is shown. It is also expected that TimeFlo must be ready for the user to start with the configuration options accessible and editable.

### TimeFlo Configuration UI Functioning

This test will consist of manually editing the break and productivity timer inputs to valid and invalid values to ensure that only values between 1 and 120 are accepted. Invalid values to be tested include: 'A', -1, 0, 121, 1, 120, 60.

Additionally, the alert volume input must be tested to ensure that it can be turned to 0% (off), 50%, and 100%.

### Start Action

This test involves manually starting the TimeFlo process using the GUI.

The expected result is that after using the start input, the productivity timer will start.

### Stop Action

This test involves manually starting the TimeFlo process using the GUI and then manually stopping it via the GUI.

The expected result is that after using the start input followed by the stop input, the timer will stop.

### Visual Timer Status

This test involves manually starting the TimeFlo process using the GUI and verifying that the GUI displays the currently running timer and how much timer is left.

The expected result is that the GUI will show the productivity timer initially after starting and then after the productivity timer displayed hits 0, will show the break timer. After manually stopping the process, it is expected that the visual status will indicate to the user that no timer is running.

### Auditory Alert Sound

This test involves starting the TimeFlo process via the GUI and letting each timer hit 0 and listening for a sound to play. For each timer that finishes, change the alert sound volume.

It is expected that a sound will play each time a timer hits 0. It is also expected that the volume input will accurately affect the volume of the played sound.

## Inspection

Go through the requirements, design, plan, and V&V docs to ensure that they are consistent. Finally look over code and ensure that the code reflects the requirements and design of TimeFlo.
