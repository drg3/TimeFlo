# The TimeFlo Project: Project Plan
*Daniel Gerendasy 2021*

## Resources

The following are resources required to execute this project.

 - Windows 10 computer for development
   - Computer
   - Display
   - Keyboard and mouse/trackpad
 - Microsoft Visual Studio IDE
   - .NET Desktop Development workload
 - Windows 10 environment for testing

## Work Breakdown

1. Create the requirements doc
2. Create the project plan
3. Create the design doc
4. Implement TimeFlo
5. Create V&V plan
6. Validate TimeFlo

## Schedule

Milestone 1: Complete by end of 11/5

Milestone 2: Complete by end of 11/8

## Milestones and Deliverables

1. Tasks 1-4 of Work Breakdown. Deliverables are the product of each task.
2. Tasks 5-6 of Work Breakdown. Deliverables are the product of each task.
