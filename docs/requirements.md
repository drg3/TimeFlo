# The TimeFlo Project: Requirements Document
*Daniel Gerendasy 2021*

## Introduction

This document describes the requirements for the TimeFlo software project. Each requirement must be met in the implementation and testing of TimeFlo unless otherwise stated.

The requirements have been separated into different sections. 

### Purpose and Scope

The purpose of this document is to establish a set of requirements of the TimeFlo project without getting into specific implementation details.

### Target Audience

This document is for the internal team working on the TimeFlo project.

### Terms and Definitions

 - **TimeFlo**: The name of the software project that this document will describe the high-level functions of.
 - **Timer**: A system which counts down, second by second, from an initial value to zero.
 - **Productivity Session**: A period of time where the user is working on a given task.
 - **Break Session**: A period of time, after a productivity session, where the user temporarily stops working on any task.
 - **Productivity Timer**: The timer that represents the current productivity session.
 - **Break Timer**: The timer that represents the current break session.
 - **Stakeholder**: Individuals who are in charge of determining what the product will do and how the project will operate.
 - **User**: A human who is using the final TimeFlo software product.

## Product Overview

The TimeFlo project will result in a software application that will run two sequential timers: one indicating how long to work for and the other indicating how long to break for. The goal is to introduce regular breaks into the user's workflow, giving them time to think before coding and, effectively, improving overall productivity.

The purpose of this section is to give the reader an understanding of the product, its designers, and its users.

### Users and Stakeholders

This section outlines the stakeholders invested in this project and what their roles and interests are.

Daniel is the sole stakeholder. As the stakeholder drafting this requirements document, his role is to determine exactly what is expected of the TimeFlo software and of the project as a whole. Daniel's interest in the project is in using it once finished. Additionally, Daniel wants a product that he can maintain after its development.

### Use cases

This section describes the intended use of TimeFlo.

A user, Daniel, who works as a software developer and is using the TimeFlo software to be more productive.

Daniel launches the TimeFlo software and begins to configure it for his work session. He sets the working time period to 25 minutes and the break time to 5 minutes. He then sets the alert volume to 50%. Finally he begins the first working session by hitting the start button.

Daniel notices a timer start counting down from 25 minutes and something on the app indicating it was time to work. He begins to start writing some code.

After 25 minutes the alert sound starts to trigger and Daniel stops what he's doing and looks at the TimeFlo app. The break timer has already started counting down from 5 minutes and the app tells him that it's time for a break. Daniel stops working and reflects on what he's done in the past 25 minutes. Eventually, the break timer triggers and Daniel hears the cue to start working again. He looks over at the app and the timer has started counting down from 25 minutes and the app tells him it is time to work again.

This happens a few more times before Daniel finishes the task he set out to finish. The productivity timer is still counting down but Daniel decides that he's done and clicks the stop button. The timer stops and the app informs him that it is no longer running.

## Functional Requirements

This section outlines features of the TimeFlo software application that must be implemented for the software application to meet the expectations of the stakeholders. The context for these requirements is within the use of the software application itself and not how the software is developed.

### Two Timers

The TimeFlo software application must implement two timers. One will be labelled as the productivity timer and the other as the break timer. These timers will function as a timer should, where an initial time period is set and then the timer decrements after each second before finally hitting 0.

Requirements:
 - One productivity timer
 - One break timer
 - Configurable timer durations for each timer

### Visual Timer Status

At any point during the running of either timer, the user must be informed which timer is running and how much time is left before the timer finishes.

If no timer is running, the user should be informed that the process is no longer running.

### Auditory Timer Alert

When any one of the timers (productivity or break) hits 0, an auditory alert must play so as to indicate to the user that the timer has completed. The volume of the sound must be configurable.

### Alternating Timers

The productivity and break timers must never run at the same time. Instead, the break timer will begin immediately after the productivity timer has finished. Then the productivity timer will begin right after the break timer has finished. This must loop until the user tells the TimeFlo program to stop.

### Start and Stop Interactions

The user must be able to start the process of alternating timers. When the user starts the process, the first timer to begin must always be the productivity timer.

The user must also be able to stop the timers at any time. If the user chooses to stop, whichever timer is currently running will stop and the process of alternating timers will also stop. No sound alert will trigger in this case.

### Graphical User Interface

The TimeFlo software application must provide a graphical user interface to the user. This interface must allow the user to interact with all the expected functionality of the application.

This includes:
 - Setting the productivity timer duration to values between 1 and 120 minutes
 - Setting the break timer duration to values between 1 and 120 minutes
 - Setting the alert volume to values between 0% and 100%
 - Starting the productivity timer
 - Stopping the timers
 - Seeing the current time left for the active timer
 - Seeing which timer is currently running

## Extra-functional Requirements

This section outlines the requirements of the project as whole that don't define functional requirements of the software application. This includes specifications for the project itself like time scope and target platform.

### Accuracy

Both the productivity and break timers must be accurate to within half a second per 5 minutes.

### Target Operating System

The application must be able to run in Windows 10. This is required to satisfy the first use case, where the user Daniel is using Windows 10.

### Target Programming Language

The application source code must be written in Microsoft's C# programming language. This is required for long-term maintainability, as the sole stakeholder is familiar with C# and wants to be able to maintain the project themselves if necessary.

### Source Code Comments

The source code must be well commented. This is required for long-term maintainability.

### Time Scope

The project must be completed, including all project docs, software that satisfies all functional and extra-functional requirements as well as passing V&V by November 9th, 2021.
