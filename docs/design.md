# The TimeFlo Project: Design
*Daniel Gerendasy 2021*

## Introduction

This document will outline the architecture and data flow of the TimeFlo project. 

The project must use WinForms to build a GUI. The logic handling state must be isolated to its own class, separate from the UI logic. The two will interact using callbacks so that state is properly reflected in both the logical state and the GUI state.

## Architecture

This project will use WinForms to provide a user friendly GUI for the user. WinForms is a GUI framework that Microsoft introduced in 2002. The architecture is event-driven. Input components are added and configured. The code registers handlers to event callbacks that are triggered when a user interacts with the component.

### Logic and User Interface

Logic will be placed in classes separate from the UI. Decoupling the logic and user interface will allow for unit testing. Callbacks triggered by the WinForms GUI will simply invoke a function in a separate class. If the state changes, the class will raise an appropriate event, triggering a handler in the user interface that will then update the GUI state to reflect the new state.

The biggest downside is that this adds a level of complexity to code and can make it harder to follow and debug. But the benefit of having a way to test the logic programmatically outweighs the added complexity.

Ideally, the GUI state and the logic state would be one but that's the cost of using WinForms over some of the newer frameworks.

### Timer Component

The timer component will function to start, stop, and indicate (via callbacks) when the timer has reached 0. This component will also expose some important state variables such as whether the timer is running and, if so, how many seconds have passed since the timer started.

### TimeFlo Component

The TimeFlo component will use two timer components to alternate between a productivity session countdown and a break session countdown. To do this, the component will keep track of which timer is currently running. When the currently running timer hits 0, it will stop the timer, choose the next timer, and start it. It will also raise an event for whenever a productivity or break timer finishes. This component will expose methods to change the duration of a productivity session and a break session, start and stop the currently running timer and exit the TimeFlo process, and get the currently running timer and how many seconds it has left before it finishes.

### User Interface Component

The user interface will be built using Visual Studio's integrated forms designer. Inputs such as buttons and numeric boxes will have event callbacks that trigger on user interaction. These callbacks will simply act to pass the inputs to the TimeFlo component.

The user interface will also handle playing the alert sound when one of the timer hits 0. To do this, it will subscribe to the TimeFlo component's events that trigger when each of the timers finish. On invocation, it will play the alert sound.

This component will display state information to the user. To do this, the form will create a timer that will periodically trigger. On each trigger, the GUI will be updated to reflect information about the current state of TimeFlo.

There will be 3 states:

    1. TimeFlo is not running and is awaiting user activation. No timer state will be shown. Timer duration inputs, start button, and volume input will be active. Stop button will be inactive.
    2. TimeFlo is running, productivity timer is counting down. Timer duration inputs and start button will be inactive. Stop button and volume input will be active.
    3. TimeFlo is running, break timer is counting down. Timer duration inputs and start button will be inactive. Stop button and volume input will be active.
